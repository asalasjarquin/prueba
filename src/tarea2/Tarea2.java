/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Dricson
 */
public class Tarea2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sn = new Scanner(System.in);

        //Indica si salimos o no
        boolean salir = false;
        int opcion;

        //Bucle para pedir las opciones hasta que elijamos salir
        while (!salir) {

            //opciones
            System.out.println("--------------------");
            System.out.println("MENU PRINCIPAL");
            System.out.println("1. Realizar entrega");
            System.out.println("2. Salir");
            System.out.println("--------------------");
            //controla la excepcion en caso de que se introduzca un valor no correcto.
            try {

                //Pide una opcion
                System.out.println("Seleccione una opcion: ");
                opcion = sn.nextInt();

                //Realiza una de las opciones
                switch (opcion) {
                    case 1:
                        int inventario = 0;
                        int vacunas = 1000;
                        for (int i = vacunas; i >= 1;) {
                            int resta = 0;

                            Scanner var = new Scanner(System.in);
                            System.out.println("Cuantas vacunas desea entregar.");
                            resta = var.nextInt();
                            inventario = (vacunas - resta);
                            vacunas = inventario;
                            if (inventario<0){
                                System.out.println("*******************************************************");
                                System.out.println("'ERROR'no puede sobre pasar los limites del inventario.");
                                System.out.println("*******************************************************");
                                break;
                            }
                            System.out.println("----------------------------------");
                            System.out.println("Quedan " + inventario + " vacunas.");
                            System.out.println("----------------------------------");
                            if (inventario < 200) {
                                if (vacunas == 0) {
                                    System.out.println("Las vacunas se han agotado,vuelva mañana.");
                                    break;
                                }
                                System.out.println("************************************************");
                                System.out.println("'ALERTA'El inventario  a bajado de 200 unidades.");
                                System.out.println("************************************************");
                            }
                        }
                        break;

                    case 2:
                        salir = true;
                        break;
                    default:
                        System.out.println("Las opciones son entre 1 y 2");
                }

                //controla la excepcion en caso de que se introduzca un valor no correcto
            } catch (InputMismatchException e) {
                System.out.println("Debes escribir un numero");
                sn.next();
            }

        }

        System.out.println("Fin del menu");

    }

}
